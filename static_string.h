#pragma once

#include <utility>
#include <array>

namespace reflect
{
    template <const char ...c>
    struct static_string
    {
        constexpr static auto lenght()
        {
            return (0 + ... + sizeof(c));
        }

        constexpr static const char* c_str()
        {
            return c_str_array.data();
        }

        constexpr static auto get_array_null_terminated()
        {
            return std::array<const char, lenght() + 1>{ {c..., '\0'} };
        }

        constexpr static inline auto c_str_array = get_array_null_terminated();
    };

    namespace static_string_detail
    {
        template<class S, auto... Ns>
        constexpr auto make_static_string_impl(S s, std::index_sequence<Ns...>)
        {
            return static_string<s.get().first[Ns]...>();
        }
    }

    template<class S>
    constexpr auto make_static_string(S s)
    {
        using namespace static_string_detail;
        return make_static_string_impl(s, std::make_index_sequence<s.get().second>());
    }

    template<class T>
    struct StringTypeOf
    {
        using type = std::add_const_t<decltype(std::declval<T>().get())>;
    };

    template<class T>
    using extract_static_string_t = std::add_const_t<decltype(std::declval<T>().get())>;
}


#define MAKE_STATIC_STR(s_str) \
    reflect::make_static_string( \
        [](){ \
            struct { \
                constexpr static auto get() \
                { \
                    return std::make_pair(s_str, sizeof(s_str)-1); \
                } \
            } _; \
            return _; \
        }())
