#pragma once

#include <type_traits>
#include <algorithm>

namespace reflect
{
    template<class Derived, class EnumType>
    struct EnumInterface
    {
    public:
        using UT = std::underlying_type_t<EnumType>;

        constexpr std::size_t count() const noexcept
        {
            return Derived::enumEntries.size();
        }

        constexpr auto begin() const noexcept
        {
            return Derived::enumEntries.cbegin();
        }

        constexpr auto end() const noexcept
        {
            return Derived::enumEntries.cend();
        }

        constexpr const char* ToString() const noexcept
        {
            return Derived::enumName;
        }

        template<EnumType Tvalue>
        constexpr static const char* ToString() noexcept
        {
            constexpr auto it = std::find(Derived::enumEntries.begin(), Derived::enumEntries.end(), Tvalue);
            static_assert (it != Derived::enumEntries.end());

            constexpr auto index = std::distance(Derived::enumEntries.begin(), it);
            return Derived::enumNames[index];
        }

        constexpr const char* ToString(EnumType value) const noexcept
        {
            auto it = std::find(Derived::enumEntries.begin(), Derived::enumEntries.end(), value);
            if( it != Derived::enumEntries.end())
            {
                auto index = std::distance(Derived::enumEntries.begin(), it);
                return Derived::enumNames[index];
            }

            return nullptr;
        }

        constexpr bool IsValid(const UT value) const noexcept
        {
            const auto it = std::find_if(Derived::enumEntries.cbegin(),
                                         Derived::enumEntries.cend(),
                                         [value](const auto& v)
                                         {
                                              return static_cast<UT>(v) == value;
                                         });
            return it != Derived::enumEntries.cend();
        }

        constexpr auto GetTypeID() const noexcept
        {
            return Derived::typeID;
        }
    };
}

