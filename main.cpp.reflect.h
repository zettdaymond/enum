#pragma once

#include "static_string.h"
#include "enum_interface.h"
#include "reflect_database.h"


namespace reflect_database
{
    namespace main__::eRoles {
        constexpr static auto typeID = MAKE_STATIC_STR("main()::eRoles");
        using TypeID = decltype (typeID);
    }

    template<class T>
    struct EnumReflection<T, main__::eRoles::TypeID>
            : public reflect::EnumInterface<EnumReflection<T, main__::eRoles::TypeID>,
                              T>
    {
    private:
        static inline constexpr std::array enumEntries = {
            static_cast<T>(10),
            static_cast<T>(100),
            static_cast<T>(230),
            static_cast<T>(439)
        };

        static inline constexpr std::array enumNames = {
            "kManager",
            "kProgrammer",
            "kArtist",
            "kQA"
        };

        constexpr static inline auto typeID = main__::eRoles::typeID;
        constexpr static inline const char* enumName = "eRoles";

        template <class Tt, class Ee>
        friend struct reflect::EnumInterface;
    };



    namespace TestStruct::TestEnum {
        constexpr static auto typeID = MAKE_STATIC_STR("TestStruct::TestEnum");
        using TypeID = decltype (typeID);
    }

    template<class T>
    struct EnumReflection<T, TestStruct::TestEnum::TypeID>
            : public reflect::EnumInterface<EnumReflection<T, TestStruct::TestEnum::TypeID>,
                              T>
    {
    private:
        static inline constexpr std::array enumEntries = {
            static_cast<T>(1),
            static_cast<T>(2)
        };

        static inline constexpr std::array enumNames = {
            "kBilinear",
            "kAnisotropic",
        };

        constexpr static inline auto typeID = TestStruct::TestEnum::typeID;
        constexpr static inline const char* enumName = "TestEnum";

        template <class Tt, class Ee>
        friend struct reflect::EnumInterface;
    };
}
