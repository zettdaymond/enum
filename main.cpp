#include <iostream>
#include <cassert>

#include "reflect.h"
#include "main.cpp.reflect.h"

using namespace std;

struct TestStruct
{
    void test()
    {
        auto testReflect = reflect::getReflection<TestStruct::TestEnum>();

        std::cout << "\n" << testReflect.ToString() << " reflection: \n";
        std::cout << "\n" << "typeid: " << testReflect.GetTypeID().c_str() << "\n";

        for(const auto& enumEntry : testReflect)
        {
            std::cout << testReflect.ToString(enumEntry) << std::endl;
        }

        bool isValidValue = testReflect.IsValid(17);
        assert (isValidValue == false);

        isValidValue = testReflect.IsValid(2);
        assert (isValidValue == true);
    }

private:
    enum class [[reflect]]
    TestEnum
    {
        kBilinear = 1,
        kAnisotropic = 2
    };
};

int main()
{
    enum class [[reflect]]
    eRoles
    {
        kManager = 10,
        kProgrammer = 100,
        kArtist = 230,
        kQA = 439
    };

    {
        auto rolesReflection = reflect::getReflection<eRoles>();

        std::cout << rolesReflection.ToString() << " reflection: \n";
        std::cout << "\n" << "typeid: " << rolesReflection.GetTypeID().c_str() << "\n";

        for(const auto& enumEntry : rolesReflection)
        {
            std::cout << rolesReflection.ToString(enumEntry) << std::endl;
        }

        bool isValidValue = rolesReflection.IsValid(17);
        assert (isValidValue == false);

        isValidValue = rolesReflection.IsValid(439);
        assert (isValidValue == true);
    }

    TestStruct t;
    t.test();

    return 0;
}
