#pragma once

#include <type_traits>

#include "type_id.h"
#include "reflect_database.h"

namespace reflect
{
    template <class T>
    auto getReflection()
    {
        using namespace reflect_database;

        if constexpr(std::is_enum_v<T>)
        {
            using TypeStaticStringID = std::add_const_t<decltype (StaticStringTypeID<T>())>;
            return EnumReflection<T, TypeStaticStringID>{};
        }
        else{
            return 3;
        }
    }

}
