cmake_minimum_required(VERSION 3.5)

project(enum LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS ON)

add_executable(enum main.cpp)
target_include_directories(enum PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
