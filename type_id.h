#pragma once

#include "static_string.h"

namespace reflect
{
    constexpr static auto kStartType = "static constexpr auto reflect::StaticStringTypeID()::<lambda()>::<unnamed struct>::get() [with T = ";

    template <typename T>
    constexpr const char* TemplatePrint()
    {
        return __PRETTY_FUNCTION__;
    }

    template<class T>
    constexpr auto StaticStringTypeID()
    {
        return make_static_string([](){
            struct {
                constexpr static auto get()
                {
                    constexpr auto text = __PRETTY_FUNCTION__;

                    std::string_view textView{text};
                    textView.remove_prefix(std::string_view{kStartType}.size());
                    textView.remove_suffix(1); // remove ']'

                    return std::make_pair(textView.data(), textView.length());
                }
            } _;
            return _;
        }());
    }


    template<class T>
    constexpr std::size_t TypeHash()
    {
        std::size_t result {0};

        auto typeString = TemplatePrint<T>();

        for (const auto &c : std::string_view{typeString})
        {
            (result ^= c) <<= 1;
        }

        return result;
    }
}
